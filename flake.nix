{
  description = "A simple cli utility for accessing PWSafe3 databases";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      flake-utils,
      naersk,
      nixpkgs,
      rust-overlay,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        inherit (pkgs.stdenv) isDarwin;

        # this overlay gives us pre-built binaries for the rust
        # toolchain, so we have reproducible builds
        overlays = [ (import rust-overlay) ];

        pkgs = (import nixpkgs) { inherit system overlays; };

        myrust = pkgs.rust-bin.stable.latest.default.override {
          extensions = [
            "rust-analyzer"
            "rust-src"
          ];
        };

        # configure naersk to use our rust overlay
        naersk' = pkgs.callPackage naersk {
          cargo = myrust;
          rustc = myrust;
        };

      in
      with pkgs;
      {
        # For `nix build` & `nix run`:
        packages.default = naersk'.buildPackage {
          nativeBuildInputs =
            [ myrust ]
            ++ lib.optionals stdenv.isLinux ([
              python3
              xorg.libxcb
            ])
            ++ lib.optionals stdenv.isDarwin ([ apple_sdk.frameworks.AppKit ]);
          src = ./.;
        };

        # For `nix develop`:
        devShells.default = pkgs.mkShell {
          nativeBuildInputs =
            [ myrust ]
            ++ lib.optionals stdenv.isLinux ([
              python3
              xorg.libxcb
            ])
            ++ lib.optionals stdenv.isDarwin ([ apple_sdk.frameworks.AppKit ]);
        };
      }
    );
}
